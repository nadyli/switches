// "Database"
const db = require("./db");

const express = require('express');

// Bodyparser
bodyparser = require('body-parser')
app = express();
app.use(bodyparser.json());

// Cache
cachectrl = require('express-cache-control');
cache = new cachectrl().middleware;

// Server PORT
SERVER_PORT = 8888;

// Swagger
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "Switch API",
      description: "API for switches",
      contact: {
        name: "Juha Oinonen"
      },
      server: ["http://localhost:8888"],
      version: "1.0"
    }
  },
  apis: ["server.js"]
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api/docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));

// Schema for switch
/**
 * @swagger
 * definitions:
 *  Switch:
 *    properties:
 *      name:
 *        type: string
 *      value:
 *        type: boolean
 */

// Routes
/**
 * @swagger
 * /api/switches/:
 *  head:
 *    description: Requesting a response identical to GET, but without reponse body on all switches
 *    responses:
 *      '200':
 *        description: A successfull response
 */
app.head('/api/switches', (req, res) => {
  res.sendStatus(200);
});

/**
 * @swagger
 * /api/switches/{id}:
 *  head:
 *    description: Requesting a response identical to GET, but without reponse body on specific switch
 *    parameters:
 *      - name: id
 *        description: Switch ID
 *        in: path
 *        required: true
 *        type: integer
 *    responses:
 *      '200':
 *        description: A successfull response
 *      '404':
 *        description: No switch found with that name/id
 */
app.head('/api/switches/:id', async (req, res) => {
  let switchData = req.params.id;
  let number = parseInt(switchData);
  if (isNaN(number)) {
    let resource = await db.getToggleByName(switchData);
    if (resource === false) {
      res.sendStatus(404);
    } else {
      res.sendStatus(200);
    }
  } else {
    let resource = await db.getToggleByID(number);
    if (resource === false) {
      res.sendStatus(404);
    } else {
      res.sendStatus(200);
    }
  }
});

/**
 * @swagger
 * /api/switches/:
 *  options:
 *    description: Use to check what HTTP-methods are available
 *    responses:
 *      '204':
 *        description: A successfull response
 */
app.options('/api/switches', (req, res) => {
  res.setHeader('Allow', 'GET,HEAD,OPTIONS,POST,PUT');
  res.setHeader('Content-Type', 'application/json');
  res.sendStatus(204);
});

/**
 * @swagger
 * /api/switches/{id}:
 *  options:
 *    description: Use to check what HTTP-methods are available on specific switch
 *    parameters:
 *      - name: id
 *        description: Switch ID
 *        in: path
 *        required: true
 *        type: integer
 *    responses:
 *      '204':
 *        description: A successfull response
 */
app.options('/api/switches/:id', async (req, res) => {
  let switchData = req.params.id;
  let number = parseInt(switchData);
  if (isNaN(number)) {
    let resource = await db.getToggleByName(switchData);
    if (resource === false) {
      res.setHeader('Allow', 'OPTIONS');
    } else {
      res.setHeader('Allow', 'GET,PUT,HEAD,OPTIONS');
      res.setHeader('Content-Type', 'application/json');
      res.setHeader('Cache-Control', 'max-age=600');
    }
  } else {
    let resource = await db.getToggleByID(number);
    if (resource === false) {
      res.setHeader('Allow', 'OPTIONS');
    } else {
      res.setHeader('Allow', 'GET,PUT,HEAD,OPTIONS');
      res.setHeader('Content-Type', 'application/json');
      res.setHeader('Cache-Control', 'max-age=600')
    }
  }
  res.sendStatus(204);
});

/**
 * @swagger
 * /api/switches/:
 *  get:
 *    description: Use to request all switches
 *    produces:
 *      - application/json
 *    responses:
 *      '200':
 *        description: A successfull response       
 */
app.get('/api/switches', cache('minutes', 10), async (req, res) => {
  let switches = await db.getToggles();
  res.status(200).json(switches);
});

/**
 * @swagger
 * /api/switches/{id}:
 *  get:
 *    description: Use to request a specific switch state with a name or whole switch info with id
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: id
 *        description: Switch ID
 *        in: path
 *        required: true
 *        type: integer
 *    responses:
 *      '200':
 *        description: A successfull response 
 *      '404':
 *        description: No switch found with that name/id
 *        schema:
 *          $ref: '#/definitions/Switch'    
 */
app.get('/api/switches/:id', cache('minutes', 10), async (req, res) => {
  let switchData = req.params.id;
  let number = parseInt(switchData);
  if (isNaN(number)) {
    let resource = await db.getToggleByName(switchData);
    if (resource === false) {
      res.sendStatus(404);
    } else {
      res.json(resource.value);
    }
  } else {
    let resource = await db.getToggleByID(number);
    if (resource === false) {
      res.sendStatus(404);
    } else {
      res.json(resource);
    }
  }
});

/**
 * @swagger
 * /api/switches/:
 *  post:
 *    description: Creates a new switch
 *    parameters:
 *      - name: Switch name
 *        description: Switch object
 *        in: body
 *        value: false
 *        required: true
 *        schema:
 *          $ref: '#/definitions/Switch'
 *    responses:
 *      '201':
 *        description: Switch created succesfully
 *      '400':
 *        description: There was a problem creating a new switch. Probably missing some parameters.      
 */
app.post('/api/switches/', async (req, res) => {
  let data = req.body;
  let success = await db.addToggle(data);
  if (success) {
    res.sendStatus(201);
  } else {
    res.sendStatus(400);
  }
});

/**
 * @swagger
 * /api/switches/{name}:
 *  put:
 *    description: Used to toggle a switch with specific name
 *    parameters:
 *      - name: name
 *        description: Switch name
 *        in: path
 *        required: true
 *        type: string
 *    responses:
 *      '204':
 *        description: Switch toggled succesfully
 *      '404':
 *        description: No switch found with that name      
 */
app.put('/api/switches/:name', async (req, res) => {
  let switchData = await db.getToggleByName(req.params.name);

  if (switchData == false) {
    return res.sendStatus(404);
  }

  let success = await db.updateToggle(switchData.id);

  if (success) {
    res.sendStatus(204);
  } else {
    res.sendStatus(404);
  }
});

app.listen(SERVER_PORT, () => {
  console.log("Server on. Awaiting requests.");
});
