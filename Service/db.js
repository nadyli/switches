// Mock-database
var toggles = [
  {
    id: 0,
    name: "lights",
    value: false
  },
  {
    id: 1,
    name: "ventilation",
    value: true
  },
  {
    id: 2,
    name: "weapons",
    value: false
  },
  {
    id: 3,
    name: "shields",
    value: true
  },
  {
    id: 4,
    name: "doors",
    value: false
  },
  {
    id: 5,
    name: "heating",
    value: false
  }
];

// FIND, ADD, UPDATE, REMOVE
async function getToggles(id) {
  return toggles;
}

async function getToggleIndex(id) {
  let index = toggles.findIndex(element => element.id === id);
  if (index !== undefined) return index;
  return -1;
}

async function getToggleByID(id) {
  let toggle = toggles.find(element => element.id === id);
  if (toggle !== undefined) return toggle;
  return false;
}

async function getToggleByName(name) {
  let toggle = toggles.find(element => element.name === name);
  if (toggle !== undefined) return toggle;
  return false;
}

async function addToggle(newData) {
  if (!newData.name || (newData.value !== true && newData.value !== false)) {
    return false;
  }

  let lastIndex = toggles.length - 1;
  let lastElement = toggles[lastIndex];

  let toggle = {
    id: lastElement.id + 1,
    name: newData.name,
    value: newData.value
  }

  toggles.push(toggle);
  return true;
}

async function updateToggle(id) {
  let toggleIndex = await getToggleIndex(id);
  if (toggleIndex === -1) {
    console.log("Error updating toggle. Toggle not found.");
    return false;
  }
  toggles[toggleIndex].value = !toggles[toggleIndex].value;
  return true;
}

async function removeToggle(id) {
  let toggleIndex = await getToggleIndex(id);
  if (toggleIndex === -1) {
    console.log("Error removing toggle. Toggle not found.");
    return false;
  }

  toggles.splice(toggleIndex, 1);
  return true;
}

module.exports.getToggles = getToggles;
module.exports.getToggleByID = getToggleByID;
module.exports.getToggleByName = getToggleByName;
module.exports.updateToggle = updateToggle;
module.exports.addToggle = addToggle;
module.exports.removeToggle = removeToggle;