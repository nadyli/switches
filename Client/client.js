const axios = require('axios')

let api = 'http://localhost:8888/api/switches'

async function main() {
    // Checking for available HTTP methods
    try {
        console.log('Checking for available HTTP methods')
        let data = await axios.options(api);
        data = data.headers.allow;
        console.log(data);
    } catch (err) {
        console.log('HTTP error code: ' + err.response.status);
    }


    // Get ALL toggles
    try {
        console.log("Trying to get all toggle data.");
        let data = await axios.get(api);
        data = data.data;
        console.log(data);
    } catch (err) {
        console.log('HTTP error code: ' + err.response.status);
    }

    // Get toggle by id = 3
    try {
        console.log("Trying to get toggle with id 3");
        let data = await axios.get(api + '/' + 3);
        data = data.data;
        console.log(data);
    } catch (err) {
        console.log('HTTP error code: ' + err.response.status);
    }

    // Create new switch
    try {
        console.log("Trying to create new switch");
        console.log("Database before adding a new toggle:");
        let request = await axios.get(api);
        let data = request.data;
        console.log(data);

        await axios.post(api, {
            name: 'timemachine',
            value: false
        });

        console.log("Database after adding a new toggle:");
        data = await axios.get(api);
        data = data.data;
        console.log(data);
    } catch (err) {
        console.log('HTTP error code: ' + err.response.status);
    }

    // Toggle a switch
    try {
        console.log("Trying to toggle the newly created switch.");
        await axios.put(api + '/timemachine');
        console.log("Checking for switch status.");
        let request = await axios.get(api + '/timemachine');
        let data = request.data;
        console.log('Switch status: ' + data);
    } catch (err) {
        console.log('HTTP error code: ' + err.response.status);
    }

    // Last check-up
    try {
        let request = await axios.get(api);
        let data = request.data;
        console.log(data);
    } catch (err) {
        console.log('HTTP error code: ' + err.response.status);
    }
}

main();