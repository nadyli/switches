A REST API Server & Client for a spaceship switchboard.

Done initially for a course project.

Also includes Swagger documentation for REST server that can be found on /api/docs/.
